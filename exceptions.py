import sys


def input_number():
    number = None
    while number is None:
        try:
            number = int(input("Give a number: "))
            print("Een mooie waarde.")
        except KeyboardInterrupt:
            print("Pauze dan maar.")
            sys.exit(1)
        except ValueError:
            print("That's not a number")
        except Exception:except Exception:  # catch all
            print("OOps, something went wrong.")
    return number


def main():
    num = input_number()
    print("Number is", num)

main()