def main():
    server_name = input("What is the server name: ")
    ip = input("What is the ip address: ")
    netmask = input("What is the netmask: ")
    
    server_properties = server_name + ',' + ip + ',' + netmask
    server_properties = f"{server_name} {ip} {netmask}"

    line_length = 12
    print('-' * line_length)
    print(server_properties)
    print('=' * line_length)

    print("Server name:", server_name)
    print("IP: ", ip)
    print("Netmask:", netmask)

    props = f"Server name: {server_name}\nIP: {ip}\nNetmask: {netmask}"
    print('-' * line_length)
    print(props)


main()
