def pretty_print_person(first_name, last_name, alias=''):
    print("Voornaam:", alias, first_name)
    print("Achternaam:", last_name)


def main():
    pretty_print_person(first_name="Guido",
                        last_name="van Rossum")


if __name__ == "__main__":
    main()
