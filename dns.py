def ask_user_hostname():
    hostname = input("Hostname: ")
    return hostname.strip()


def ask_user_ip():
    ip = input("IP: ")
    return ip.strip()


def print_dns(dns):
    for key, value in dns.items():
        print(f"{key:<30}{value:>30})

def add_to_dns(dns, hostname, ip):
    if not hostname in dns:
        dns[hostname] = ip


def user_wants_to_quit():
    do_quit = input("Quit? [Y/N] ").strip()

    if do_quit.lower() in ['y', 'yes', 'j', 'ja']: 
        return True
    else:
        return False


def user_interact(dns):
    while True:
        hostname = ask_user_hostname()
        ip = ask_user_ip()

        add_to_dns(dns, hostname, ip)
        print_dns(dns)

        if user_wants_to_quit():
            return dns


def main():
    dns = { 'rjekker.nl': '136.144.169.117' }
    dns = user_interact(dns)


if __name__ == "__main__":
    main()
