def print_indexed_names(servers):
    for index, server in enumerate(servers):
        print(index, server[0])


def ask_indices(max_index):
    indices = []
    while True:
        try:
            raw_index = input("Van welke server wil je de details zien?")
            if not raw_index.strip():
                uniques = set(indices)
                return uniques
            index = int(raw_index)
            if index >= 0 and index <= max_index:
                indices.append(index)
                '''
                more = input("Nog een server toevoegen? ")
    
                if more.strip().lower() in ['ja', 'yes', 'jawohl', 'bitte', 'oui']:
                    continue
                else:
                    uniques = set(indices)
                    return uniques
                '''
            else: 
                print("Index", index, " is niet geldig.")
        except Exception:
            print("Dat is geen geldige waarde.")


def print_properties(indices, servers):
    for index in indices:
        server = servers[index]  # mogelijk een IndexError exceptie
        print("name:", server[0])
        print("ip:", server[1])
        print("netmask:", server[2])


def main():
    servers = [["Amsterdam", '192.168.178.3', '255.255.255.0'],
		        ["Tokyo", '192.168.178.8', '255.255.255.0'],
		        ["Paris",  '192.168.178.12', '255.255.255.0'],
		        ["Helsinki",  '192.168.178.145', '255.255.255.0'],
		        ["Brussel",  '192.168.178.12', '255.255.255.0']]

    print_indexed_names(servers)
    max_index = len(servers) - 1
    indices = ask_indices(max_index)
    print_properties(indices, servers)

if __name__ == "__main__":
    main()
