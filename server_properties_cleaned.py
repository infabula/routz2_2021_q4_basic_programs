def main():
    server_name = input("What is the server name: ")
    server_name = server_name.strip()
    ip = input("What is the ip address: ")
    ip = ip.strip()
    netmask = input("What is the netmask: " )
    netmask = netmask.strip()

    server_properties = server_name + ',' + ip + ',' + netmask
    print('-' * 12)
    print(server_properties)
    print('=' * 12)
    print("Servername:", server_name)
    print("IP: ", ip)
    print("Netmask:", netmask)


main()
