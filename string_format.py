def main():
    name = "Jeroen"
    age = 42
    msg = name + ' ' + str(age)
    print(msg)

    name = "Eve"
    age = 1
    msg2 = "{naam: <20} {leeftijd:^3}".format(naam=name, leeftijd=age)
    print(msg2)

    age = 102
    name = "Jan"
    msg3 = f"{name: <20} {age:^3}"
    print(msg3)



main()