import sys 


def load_configuration(filename):
    print("Loading configuration", filename)


def save_configuration(config):
    print("Saving confuration")
    # gaat mis werp dan een exceptie


def do_exit():
    print("Exiting")
    sys.exit(0)


def what_to_do():
    command = input("What to do? [load, save, quit]")
    command = command.strip().lower()  # chaining
    #command = command.lower()  # make case insensitive

    if command == "load":
        filename = input("Filename to load:").strip()
        load_configuration(filename)
    elif command == "save":
        save_configuration()
    elif command == "quit":
        do_exit()
    else:
        print("Unkown command.")


def main():
    what_to_do()


if __name__ == "__main__":
    main()
