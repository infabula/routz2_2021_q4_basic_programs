from random import randint


def select_random_number():
    min, max = ask_min_max_numbers()
    while True:
        random_num = randint(min, max)
        print(random_num)
        response = input("Is this the random number you want?")
        if response.lower() == 'yes':
            return random_num
        print("Let's generate another number between", min, max)


def ask_min_max_numbers():
    minimum = int(input("What should be the minimum number? "))
    while True:
        maximum = int(input("What should be the maximum number? "))
        if maximum > minimum:
            break
        print("The maximum number should be larger than", min)
    
    return minimum, maximum


def main():
    foo, bar = ask_min_max_numbers()
    number = select_random_number(foo, bar)

    message = "Your selected random number between " + str(min) + " and " + str(max) + " is " + str(number) 
    print(message)


if __name__ == "__main__":
    main()
