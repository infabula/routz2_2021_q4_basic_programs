class OctetError(Exception):
    """doc string"""
    pass


def ask_number(minimum, maximum, max_tries=3):
    if maximum < minimum:
        raise ValueError("Max is smaller than min")

    for try_count in range(max_tries):
        num = int(input(": "))
        if (num >= minimum) and (num <= maximum):
            return num
        print("Tussen",  minimum, "en", maximum, "graag!")
    print("OK, nu is het klaar")
    return minimum


def generate_ip_addresses(start_octet, end_octet, base="192.168.178"):
    # sanitatie
    if start_octet > end_octet:
        raise ValueError("Start octet can't be larger than end octet")

    if start_octet not in range(0, 256):
        raise OctetError("octet out of range")

    if end_octet not in range(0, 256):
        raise OctetError("octet out of range")

    for octet in range(start_octet, end_octet):
        address = f"{base}.{octet}"
        print(address)


def main():
    print("Hoeveel ip-adressen moeten er worden gemaakt?")
    try:
        ip_count = ask_number(minimum=1, maximum=100)

        print("Wat is het start-octet?")
        start_octet = ask_number(minimum=0, maximum=300, max_tries=2)
        end = start_octet + ip_count

        generate_ip_addresses(start_octet, end)
    except OctetError:
        print("On ongeldig octet gezien.")
    except ValueError as e:
        print(e)
    except Exception as e:
        print("Iets onbekends ging mis")
        print(e)


if __name__ == "__main__":
    main()
