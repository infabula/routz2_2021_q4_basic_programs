with open("groet.txt", 'r') as infile:
    template = infile.read()


data = {'name': "Jeroen",
        'age': 42,
        'current_year': 2021,}

content = template.format(**data)  # named argument in een dictionary aangeboden
# content = template.format(name="Jeroen", age=42, current_year=2021)  # named argument in een dictionary aangeboden

print(content)