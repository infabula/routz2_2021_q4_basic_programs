import os

def ask_user_for_filename():
    return "servers.csv"

def write_servers_as_csv(folder, filname):
    if not os.path.exists(folder):
        os.makedirs(folder)
    print("weggeschreven naar", folder, filname)

def print_server(server_data):
    """
    server_data = "parijs, 192.168.1.1, 255.255.0.0"

    :param server_data:
    :return:
    """
    # zouden we niet kunnen strippen op komma?
    items = server_data.split(',')   # list
    # sanitize
    if len(items) < 3:
        raise ValueError("foutje is csv")

    name = items[0].strip()
    ip = items[1].strip()
    netmask = items[2].strip()

    msg = f"{name:<20}{ip:^20}{netmask:^20}"
    print(msg)


def print_servers_from_file(folder, filename):
    """
    # makedirs()
    # mkdir()

    u g o
    rwx rwx rwx
    110 100 100
    6   4   4
    """
    full_path = os.path.join(folder, filename)
    with open(full_path, 'r') as infile:
        for line in infile:
            data = line.rstrip()
            print_server(data)


def main():
    folder = "data"
    try:
        filename = ask_user_for_filename()
        print_servers_from_file(folder, filename)
    except Exception as e:
        print("Een exceptie:")
        print(e)

if __name__ == "__main__":
    main()