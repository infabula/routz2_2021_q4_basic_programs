import os.path

def ask_servernames_in_context(filename):
    with open(filename, 'w') as outfile:
        while True:
            raw_name = input("Server name: ")
            name = raw_name.strip()

            if len(name) == 0:
                break
            else:
                outfile.write(name + '\n')


def ask_servernames(filename):
    outfile = open(filename, 'w')

    while True:
        raw_name = input("Server name: ")
        name = raw_name.strip()

        if len(name) == 0:
            break
        print(name, file=outfile)

    outfile.close()  # don't forget


def print_names_from_file(filename):
    infile = open(filename, 'r')

    for line in infile:
        name = line.strip()
        if name:
            print(name)

    infile.close()


def main():
    try:
        filename = os.path.join('data', 'servernames.txt')
        ask_servernames(filename)
        # ask_servernames_in_context(filename)
        print_names_from_file(filename)
    except Exception as e:
        print("Something went wrong...")
        print(e)


if __name__ == "__main__":
    main()