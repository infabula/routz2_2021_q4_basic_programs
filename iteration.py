def print_a_lot():
    count = 0

    while True:  # oneindig loopen
        print(count)
        if count >= 100:
            break
        count = count + 1  # count += 1

    print("Klaar met while.")


def range_loop():
    for index in range(0, 51, 5):
        if index == 25:
            continue
        print(index)


def main():
    #print_a_lot()
    range_loop()


main()


