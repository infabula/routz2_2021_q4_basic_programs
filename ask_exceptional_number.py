def ask_number(minimum=0, max_tries=3):
    for try_count in range(max_tries):
        message = "Geef een getal vanaf " + str(minimum) + ": "
        try:
            num = int(input(message))
            if num >= minimum:
                return num
            print("Niet kleiner dan", minimum, "graag!")
        except ValueError as e:
            print("Dat is geen getal")
    raise ValueError("Gebruiker is besluiteloos")

def main():
    try:
        number = ask_number(4)
        print("U koos", number)
    except ValueError:
        print("Helaas hebben we geen getal kunnen verkrijgen.")
    except Exception as e:
        # vang alle overige excepties
        print("Een onverwachte fout is opgetreden.")
         

if __name__ == "__main__":
    main()
