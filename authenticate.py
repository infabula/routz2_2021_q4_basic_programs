import sys


def ask_username(msg):
    raw_name = input(msg)
    name = raw_name.strip()

    if len(name) < 3:
        print("Username should be at least 3 characters.")
        sys.exit(1)
    if name.contains(' ') or '\t' in name:
        print("Username should not contain spaces or tabs.")
        sys.exit(42)
    return name


def ask_password(username):
    raw_password = input("Password: ")
    password = raw_password.strip()

    if len(password) < 5:
        print("Password should be at least 8 characters.")
        sys.exit(46)

    elif username == password:
        print("Password should be different than the username.")
        sys.exit(45)
        
    return password


def is_welcome(name, passwd):
    if name == "Bob" and passwd == "secret":
        return True
    elif name == "Alice" and passwd == "12345":
        return True
    else:
        return False


def main():
    username = ask_username(msg="Gebruikersnaam:")
    password = ask_password(username)

    if is_welcome(username, password):
        message = "Welcome in " + username + "."
    else:
        message = "Not allowed."

    print(message)
    sys.exit(0)


if __name__ == "__main__":
    main()
