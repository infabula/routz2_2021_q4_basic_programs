def main():
    vps_price = float(input("What is price per month for one VPS? "))
    server_count = int(input("How many VPS servers do you need? "))
    provision_time = int(input("How many minutes needs an administrator to provision one VPS? "))

    admin_cost_per_minute = 84.00 / 60

    costs = vps_price * server_count + (admin_cost_per_minute * provision_time * server_count)
    rounded_costs = round(costs, 2)

    message = "The total cost for " + str(server_count) +  " servers is " + str(rounded_costs) + " euro."
    print(message)


main()
