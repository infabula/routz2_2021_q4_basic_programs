def ask_amount():
    """Asks the user for an amount.

    Raises ValueError if amount is not a number.
    """

    amount = int(input("How many?"))
    if amount < 0:
        raise ValueError("Amount can not be negative")
    else:
        return amount


def main():
    try:
        amount = ask_amount()
        message = f"The user wants {amount}"
    except ValueError:
        message = "The amount can not be negative, setting it to 0"
        amount = 0
    finally:
        print(message)
        print("Amount:", amount)

main()
