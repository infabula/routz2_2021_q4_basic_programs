from url_utils import build_url


def main():
    print("Mijn naam is", __name__)
    url = build_url(hostname="service.org",
                    path="/api/v2/",
                    resource="interfaces",
                    port=7700,
                    ext=".json")
    print(url)
    url = build_url("service.org", "/api/v2/", "routers")
    print(url)


if __name__ == "__main__":
    main()
