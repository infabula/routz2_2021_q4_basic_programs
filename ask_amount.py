def ask_amount():
    amount = int(input("How many?"))
    if amount < 0:
        print("That is not a positive number!")
        return 0
    else:
        return amount

def main():
    amount = ask_amount()
    message = "The user wants " + str(amount)
    print(message)

main()
