def build_url(hostname, path, resource, port=443, ext=".html"):
    url = "https://" + hostname + ":" + str(port) + path + resource + ext
    return url


# Test code
if __name__ == "__main__":
    url = build_url(hostname="service.org",
                        path="/api/v2/",
                        resource="interfaces",
                        port=7700,
                        ext=".json")
    print(url)
else:
    print("Ik word geimporteerd!!!")