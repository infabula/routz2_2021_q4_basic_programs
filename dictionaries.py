person_1 = {'name': 'Guido',
            'last_name': 'van Rossum',
            'age': 42,
            'hobbies': ['proggen', 'golf']
}

person_2 = {'name': 'John',
            'last_name': 'Spenser',
            'age': 34,
            'hobbies': ['lassen', 'lezen', 'koken']
}

dns = {'localhost': ['127.0.0.1'],
       'www.test.nl': ['168.192.0.5',
                        '888.777.666.444',
                        '123.456.789.0']
}


group = [person_1, person_2]
names = ['George', 'Eve']

# missing keys
if 'gender' in person_1:
    print("Gender van", name, "is", person_1['gender'])

gender = 0.5
if 'gender' in person_1:
    gender = person_1['gender']


gender = person_1.get('gender', 0.5)




#person_3 = get_person_from_file()

#group.append(person_3)


interfaces = [{'intf': 'FastEthernet0',
  'ipaddr': 'unassigned',
  'status': 'down',
  'proto': 'down'},
 {'intf': 'FastEthernet1',
  'ipaddr': 'unassigned',
  'status': 'down',
  'proto': 'down'},
 {'intf': 'FastEthernet2',
  'ipaddr': 'unassigned',
  'status': 'down',
  'proto': 'down'},
 {'intf': 'FastEthernet3',
  'ipaddr': 'unassigned',
  'status': 'down',
  'proto': 'down'},
 {'intf': 'FastEthernet4',
  'ipaddr': '10.220.88.20',
  'status': 'up',
  'proto': 'up'},
 {'intf': 'Vlan1', 'ipaddr': 'unassigned', 'status': 'down', 'proto': 'down'}]